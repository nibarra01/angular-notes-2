import { Injectable } from '@angular/core';
import {DCHeroEvents} from "../models/DCHeroEvents";
import  {Observable} from "rxjs";
import  { of } from 'rxjs';
/* this will allow us to return an array as an observable */


@Injectable({
  providedIn: 'root'
})
export class DataService {
  characters:  DCHeroEvents[];

  data:any;


  constructor() {
    this.characters = [
      {
        persona: 'Superman',
        firstName: 'Clark',
        lastName: 'Kent',
        age:  54,
        address:  {
          street: '27 Smallville',
          city: 'Metorpolis',
          state: 'IL'
        },
        img:'../../assets/img/644-superman.jpg',
        isActive:true,
        balance:  12300000,
        memberSince: new Date('05/01/1939 8:30:00'),
        hide:false
      },
      {
        persona:  'Raven',
        firstName:  'Rachel',
        lastName: 'Roth',
        age:  134,
        address:{
          street: '1600 Main st',
          city: 'Los Angeles',
          state: 'CA'
        },
        img:'../../assets/img/542-raven.jpg',
        hide:false,
      },
      {
        persona:  'Batman',
        firstName:  'Bruce',
        lastName: 'Wayne',
        age:  43,
        address:{
          street: '50 Wayne Manor',
          city: 'Gotham City',
          state: 'NY'
        },
        img:'../../assets/img/70-batman.jpg',
        isActive: true,
        balance: 999999999999999,
        hide:false
      }
    ];


  }

//  create a method that will return a type of DCHeroEvents []
//   getCharacters(): DCHeroEvents[]{
//     return this.characters;
//   }

  getCharacters(): Observable<DCHeroEvents[]>{
    console.log("getting characters form dataService");
    return of(this.characters);
  }
  //asynchronous

  addCharacter(character: DCHeroEvents){
    this.characters.unshift(character);
  }

  getData(){
    this.data = new Observable(observer => {
      setTimeout(()=> {
        observer.next(1);
      }, 2000);
      setTimeout(()=> {
        observer.next(2);
      }, 2000);
    });
    return this.data
}

}
