import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {NgmodelComponent} from "./components/ngmodel/ngmodel.component";
import {MoviesComponent} from "./components/movies/movies.component";
import {PostHttpClientComponent} from "./components/post-http-client/post-http-client.component";
import {BandsComponent} from "./components/bands/bands.component";

// this is where we're going to route our paths
const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  //path must be the same as your routerLink in your template minus the forward slash
  //component make sure it's the proper component
  {
    path: 'characters',
    component: NgmodelComponent,
  },
  {
    path: 'movies',
    component: MoviesComponent,
  },
  {
    path: 'posts',
    component: PostHttpClientComponent,
  },
  {
    path: 'bands',
    component: BandsComponent,
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
