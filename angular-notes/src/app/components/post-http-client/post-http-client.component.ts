import { Component, OnInit } from '@angular/core';
import {PostHttpClient} from "../../models/PostHttpClient";
import {PostService} from "../../services/post.service";

@Component({
  selector: 'app-post-http-client',
  templateUrl: './post-http-client.component.html',
  styleUrls: ['./post-http-client.component.css']
})
export class PostHttpClientComponent implements OnInit {
  //property
  posts:PostHttpClient[];
  currentPost: PostHttpClient = {
    id: 0,
    title: '',
    body: '',
  };

  //property for update button
  isEdit: boolean = false

  // inject our service as a dependency
  //Dependency Injection
  constructor(private ps: PostService) { }


  //fetch the posts when ngOnInit() method is initialized
  ngOnInit(): void {
    //subscribe to our Observable that's in our PostService
    this.ps.getPosts().subscribe(p=>{
      // console.log(p);
      this.posts = p;
    })

  }

//create onNewPost() method
  onNewPost(post: PostHttpClient){
    this.posts.unshift(post);
  }


  //create editPost() method
  editPost(post: PostHttpClient){
    this.currentPost = post;

    this.isEdit = true;
  }

  //create method for onUpdatedPost()
  onUpdatedPost(post: PostHttpClient){
    this.posts.forEach((current, index) => {
      if (post.id === current.id){

        console.log('potatp')
        this.posts.splice(index,1);
        this.posts.unshift(post);
        this.isEdit = false;
        this.currentPost = {
          id: 0,
          body: '',
          title: ''
        }
      }
    })
  }


  //create the method for removePost()
  removePost(post: PostHttpClient){
    if (confirm("are you sure")){
      this.ps.deletePost(post.id)
        .subscribe(()=>{
          this.posts.forEach((current,index)=>{
            if (post.id === current.id){
              this.posts.splice(index, 1);
            }
          })
        })
    }
  }
}
