import { Component, OnInit } from '@angular/core';
import  {DCHeroEvents} from "../../models/DCHeroEvents";

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  characters: DCHeroEvents[];
  enableAddUser: boolean;
  currentClasses: {};
  currentStyle:{}

  constructor() { }

  ngOnInit(): void {
    this.enableAddUser =false;
    //adding data og array characters
    this.characters =[
      {
        persona: 'Superman',
        firstName: 'Clark',
        lastName: 'Kent',
        age:  54,
        address:  {
          street: '27 Smallville',
          city: 'Metorpolis',
          state: 'IL'
        },
        img:'../../assets/img/644-superman.jpg',
        isActive:true,
        balance:  12300000,
        memberSince: new Date('05/01/1939 8:30:00'),
        hide:false
      },
      {
        persona:  'Raven',
        firstName:  'Rachel',
        lastName: 'Roth',
        age:  134,
        address:{
          street: '1600 Main st',
          city: 'Los Angeles',
          state: 'CA'
        },
        img:'../../assets/img/542-raven.jpg',
        hide:false,
      },
      {
        persona:  'Batman',
        firstName:  'Bruce',
        lastName: 'Wayne',
        age:  43,
        address:{
          street: '50 Wayne Manor',
          city: 'Gotham City',
          state: 'NY'
        },
        img:'../../assets/img/70-batman.jpg',
        isActive: true,
        balance: 999999999999999,
        hide:false
      }
    ] //end of array
    // this.setCurrentClasses();
    this.setCurrentStyle();
  }

  // setCurrentClasses(){
  //   this.currentClasses = {
  //     'btn-success':this.enableAddUser
  //
  //   }
  // }

  setCurrentStyle(){
    this.currentStyle = {
      'padding-top': '60px',
      'text-decoration':'underline'
    }
  }

  toggleInfo(character){
    // alert('button clicked. line 85 events.components.ts');
    character.hide =!character.hide;
  }

  lodsofemone(character){
    // console.log(character.balance);

    if (character.balance == undefined){
      character.balance = 3.5
      alert('Bout three fiddy')
    }

    character.balance += 1000;

    //methods for the types of events


    }

  triggerEvent(e){
    console.log(e.type);

  }

  showHideForm(){
    this.enableAddUser =! this.enableAddUser
  }

  addCharacter(e){

//?


  }

  fireEvent(e){
    console.log(e.type);
    console.log(e.target.value);
  }

}
