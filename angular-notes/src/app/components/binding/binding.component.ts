import { Component, OnInit } from '@angular/core';
import  {DChero} from "../../models/DChero";

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent implements OnInit {
  //properties
  characters: DChero[];
  enableAddUser: boolean;
  currentClasses:{}; // an empty object
  currentStyle:{ } //empty object

  constructor() { }

  ngOnInit(): void {
    this.enableAddUser =true;
    //adding data og array characters
    this.characters =[
      {
        persona: 'Superman',
        firstName: 'Clark',
        lastName: 'Kent',
        age:  54,
        address:  {
          street: '27 Smallville',
          city: 'Metorpolis',
          state: 'IL'
        },
        img:'../../assets/img/644-superman.jpg',
        isActive:true,
        balance:  12300000,
        memberSince: new Date('05/01/1939 8:30:00')
      },
      {
        persona:  'Raven',
        firstName:  'Rachel',
        lastName: 'Roth',
        age:  134,
        address:{
          street: '1600 Main st',
          city: 'Los Angeles',
          state: 'CA'
        },
        img:'../../assets/img/542-raven.jpg',
      },
      {
        persona:  'Batman',
        firstName:  'Bruce',
        lastName: 'Wayne',
        age:  43,
        address:{
          street: '50 Wayne Manor',
          city: 'Gotham City',
          state: 'NY'
        },
        img:'../../assets/img/70-batman.jpg',
        isActive: true,
        balance: 999999999999999
      }
      ] //end of array
    this.setCurrentClasses();
    this.setCurrentStyle();

  }

//  methods
//  ngClass
  setCurrentClasses(){
    this.currentClasses = {
      'btn-success':this.enableAddUser

    }
  }

  //ngStyle
  setCurrentStyle(){
    this.currentStyle = {
      'padding-top': '60px',
      'text-decoration':'underline'
    }
  }
}
