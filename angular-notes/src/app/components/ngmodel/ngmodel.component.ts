import { Component, OnInit, ViewChild } from '@angular/core';
import  {DCHeroEvents} from "../../models/DCHeroEvents";
import {DataService} from "../../services/data.service";


@Component({
  selector: 'app-ngmodel',
  templateUrl: './ngmodel.component.html',
  styleUrls: ['./ngmodel.component.css']
})
export class NgmodelComponent implements OnInit {

  //a property for the new character entry
  //set each property of the object to empty or null
  character: DCHeroEvents = {
    persona:  '',
    firstName:'',
    lastName:'',
    age:null,
    address:{
      street: '',
      city:'',
      state:''
    }
  }

  characters: DCHeroEvents[];
  enableAddUser: boolean;
  currentClasses: {};
  currentStyle:{}

  data:any;

  // submit lesson
  @ViewChild('characterForm')form: any;
  /*
  Calling ViewChild and passed in the name of our form 'characterForm'
  making this our "Form Identifier"
   */

  //inject the dat service in the constructor

  constructor(private dataService: DataService) { }
  /*
  private = can't be used anywhere else, only within this class
  dataService = variable
  DataService = setting our variable to the DataService that we brought in
  now we should be able to access any methods inside of our Data Service
   */

  ngOnInit(): void {
    this.enableAddUser =false;
    //adding data og array characters
    // this.characters =[
    //   {
    //     persona: 'Superman',
    //     firstName: 'Clark',
    //     lastName: 'Kent',
    //     age:  54,
    //     address:  {
    //       street: '27 Smallville',
    //       city: 'Metorpolis',
    //       state: 'IL'
    //     },
    //     img:'../../assets/img/644-superman.jpg',
    //     isActive:true,
    //     balance:  12300000,
    //     memberSince: new Date('05/01/1939 8:30:00'),
    //     hide:false
    //   },
    //   {
    //     persona:  'Raven',
    //     firstName:  'Rachel',
    //     lastName: 'Roth',
    //     age:  134,
    //     address:{
    //       street: '1600 Main st',
    //       city: 'Los Angeles',
    //       state: 'CA'
    //     },
    //     img:'../../assets/img/542-raven.jpg',
    //     hide:false,
    //   },
    //   {
    //     persona:  'Batman',
    //     firstName:  'Bruce',
    //     lastName: 'Wayne',
    //     age:  43,
    //     address:{
    //       street: '50 Wayne Manor',
    //       city: 'Gotham City',
    //       state: 'NY'
    //     },
    //     img:'../../assets/img/70-batman.jpg',
    //     isActive: true,
    //     balance: 999999999999999,
    //     hide:false
    //   }
    // ] //end of array
    this.setCurrentStyle();

    //access the getCharacters() that's inside of our DataService
    // this.characters = this.dataService.getCharacters();

    this.dataService.getCharacters().subscribe(c =>{
      this.characters = c;
    });

    //subscribe to the Observable
    this.dataService.getData().subscribe(data => {
      console.log(data)
    });


  }


  setCurrentStyle(){
    this.currentStyle = {
      'padding-top': '60px',
      'text-decoration':'underline'
    }
  }

  toggleInfo(character){
    // alert('button clicked. line 85 events.components.ts');
    character.hide =!character.hide;
  }

  lodsofemone(character){
    // console.log(character.balance);

    if (character.balance == undefined){
      character.balance = 3.5
      alert('Bout three fiddy')
    }

    character.balance += 1000;

    //methods for the types of events


  }

  triggerEvent(e){
    console.log(e.type);

  }

  showHideForm(){
    this.enableAddUser =! this.enableAddUser
  }

  addCharacter(e){

    this.characters.unshift(this.character)

    //clear out the form
    this.character = {
      persona:  '',
      firstName:'',
      lastName:'',
      age:null,
      address:{
        street: '',
        city:'',
        state:''
      }
    }


  }

  fireEvent(e){
    console.log(e.type);
    console.log(e.target.value);
  }

  // onSubmit({value, valid}:{value: DCHeroEvents, valid: boolean}){
  //   console.log("aaaaaaaaaaa")
  //   if (!valid){
  //     console.log('form is not valid')
  //   } else {
  //     console.log("inside the else statement")
  //     value.isActive = true;
  //     console.log(value.isActive)
  //     value.memberSince = new Date();
  //     value.hide= true;
  //     console.log(this.character);
  //     console.log(this.characters);
  //   }
  //   console.log(value.isActive)
  //   console.log(this.characters)
  // }

//passing in an object as our method's arguement
//  value is set as a DCHeroEvents Type
//  valid is set as a boolean type
}
