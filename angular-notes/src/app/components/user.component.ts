import {Component} from "@angular/core";
import {collectExternalReferences} from "@angular/compiler";
import {inspect} from "util";

@Component({
  selector: 'app-user',
  // template: '<h3>Jane Doe</h3>'
  templateUrl: './user.component.html',
  styleUrls:  ['./user.component.css']
})

export class UserComponent{
  fname: string;
  lname: string;
  age: number;

  constructor() {
    this.fname = 'Charlie';
    this.lname = 'Cohort';
    this.age = 33;
  }

  //method
  greeting(){
    console.log(`Hello from ${this.fname} ${this.lname}, I am ${this.age} years old.`)
  }

}
