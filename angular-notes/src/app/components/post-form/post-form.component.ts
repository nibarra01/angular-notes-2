import {Component, OnInit, EventEmitter, Output, Input} from '@angular/core';
import { PostHttpClient} from "../../models/PostHttpClient";
import  {PostService} from "../../services/post.service";

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  //properties
  post: PostHttpClient;
  @Output( ) newPost: EventEmitter<PostHttpClient> = new EventEmitter();

  @Output() updatedPost: EventEmitter<PostHttpClient> = new EventEmitter();


  // define currentPost as Input
  @Input() currentPost: PostHttpClient;

  //define isEdit as input
  @Input() isEdit: boolean;

  //inject our dependency
  constructor(private ps: PostService) { }

  ngOnInit(): void {
  }

  // addPost(title, body){
  //   // console.log(title);
  //   // console.log(body);
  //
  //     if (!title || !body){
  //       alert('Please add to entry')
  //     } else {
  //       console.log(title, body);
  //
  //       this.ps.savePost({title, body} as PostHttpClient)
  //         .subscribe(post => {
  //              console.log(post);
  //       })
  //     }
  //
  // }

  //update our addPost() method by attaching the event emitter
  addPost(title, body){
    if (!title || !body){
      alert('Please add to entry')
    } else {
      console.log(title, body);

    this.ps.savePost({title, body} as PostHttpClient)
      .subscribe(post => {
        // console.log(post);
        this.newPost.emit(post);

      })
  }

}

//create method for updatePost()
  updatePost(){
    // console.log('updating post')

    this.ps.updatePost(this.currentPost)
      .subscribe(p =>{
      this.isEdit = false;
      this.updatedPost.emit(p);
    })
  }

}
