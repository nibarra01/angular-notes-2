import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {

  num1: number;
  num2: number;



  constructor() {
    // const addButton = document.getElementById("add");
    // const subtractButton = document.getElementById("minus");
    // const multiplyButton = document.getElementById("times");
    // const divideButton = document.getElementById("divide");
    // addButton.addEventListener('click',);
    // subtractButton.addEventListener('click');
    // multiplyButton.addEventListener('click');
    // divideButton.addEventListener('click');

    this.addition(4,8);
    this.subtraction(10,7);
    this.multiplication(5,8);
    this.division(36, 2 );
    this.fizzbuzz();

  }

  ngOnInit(): void {
  }
  addition(a,b){
    this.num1 = a;
    this.num2 = b;
    // console.log(`${a} plus ${b} is ${a+b}`);
    return (`${a} plus ${b} is ${a+b}`);

  }
  subtraction(a,b){
    this.num1 = a;
    this.num2 = b;
    // console.log(`${a} minus ${b} is ${a-b}`);
    return (`${a} minus ${b} is ${a-b}`);

  }
  multiplication(a,b){
    this.num1 = a;
    this.num2 = b;
    // console.log(`${a} times ${b} is ${a*b}`);
    return (`${a} times ${b} is ${a*b}`);

  }
  division(a,b){
    this.num1 = a;
    this.num2 = b;
    // console.log(`${a} divided by ${b} is ${a/b}`);
    return (`${a} divided ${b} is ${a/b}`);

  }

  // getNumbers(){
  //   this.num1 = parseInt(prompt("Input number 'A'"));
  //   this.num2 = parseInt(prompt("Input number 'B'"));
  // }

  fizzbuzz(){
    for (var n = 1; n <= 100; n++){
      if (n %3 ==0 && n%5==0){
        console.log('FizzBuzz');
      } else if( n%3 == 0){
        console.log('Fizz');
      }else if (n%5 == 0 ){
        console.log('Buzz');
      } else {
        console.log(n);
      }
    }
  }

}
