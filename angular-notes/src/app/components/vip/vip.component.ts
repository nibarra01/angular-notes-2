import { Component, OnInit } from '@angular/core';
import { Vip} from "../../models/vip";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.component.html',
  styleUrls: ['./vip.component.css']
})
export class VipComponent implements OnInit {

  members: Vip[];
  load: any;

  constructor() { }

  ngOnInit(): void {
    this.members = [{
      name: 'Test Name 1',
      username: 'Test_Username_1',
      memberNo:1
    },
      {
        name: 'Test Name 2',
        username: 'Test_Username_2',
        memberNo:2
      },
      {
        name: 'Test Name 3',
        username: 'Test_Username_3',
        memberNo:3
      },
      {
        name: 'Test Name 4',
        username: 'Test_Username_4',
        memberNo:4
      },
      {
        name: 'Test Name 5',
        username: 'Test_Username_5',
        memberNo:5
      },
    ];


    this.load = setTimeout(function (){console.log('tomato')}, 1000);
    console.log(this.load);
  }



}
