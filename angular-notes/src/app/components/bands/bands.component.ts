import { Component, OnInit } from '@angular/core';
import { Band} from "../../models/Band";

@Component({
  selector: 'app-bands',
  templateUrl: './bands.component.html',
  styleUrls: ['./bands.component.css']
})
export class BandsComponent implements OnInit {
  bands: Band[];
  toggler: boolean;
  newBand: Band;
  newAlbum1: {
    name: string,
    year: number
  };
  newAlbum2: {
    name: string,
    year: number
  };
  newAlbum3: {
    name: string,
    year: number
  };

  constructor() {

  }

  ngOnInit(): void {
    this.toggler = false;
    this.bands = [
      {
        name: 'Powerglove',
        yearEstablished: 2005,
        vocalist: 'Tony Kakko (Saturday Morning Apocalypse)',
        vocalist2: 'Marc Hudson (Continue?)',
        guitarist: 'Alex Berkson',
        guitarist2: 'Ben Cohen',
        guitarist3: 'Matt Piggot (Metal Kombat for the Mortal Man)',
        bass:'Adam Spalding (Total Pwnage)',
        drums: 'Bassil SIlver',
        synth: 'Alex Berkson',
        synth2: 'Bassil Silver',
        synth3: 'Matt Piggot (Metal Kombat for the Mortal Man)',
        saxophone: 'Simon Jeker (Metal Kombat for the Mortal Man)',
        albums: [{
          name:'Metal Kombat for the Mortal Man',
          year: 2007
        },
          {
            name: 'Saturday Morning Apocalypse',
            year: 2010
          },
          {
            name: 'Continue?',
            year: 2018
          }]
      },
      {
        name: 'They Might Be Giants',
        yearEstablished: 1982,
        vocalist: 'John Flansburgh',
        vocalist2: 'John Linnell',
        vocalist3: 'Dan Miller',
        guitarist: 'John Flansburgh',
        guitarist2: 'Dan Miller',
        bass: 'Danny Weinkauf',
        drums: 'Marty Beller',
        keyboard: 'John Linnell',
        keyboard2: 'Dan Miller',
        keyboard3: 'Danny Weinkauf',
        albums:[{
          name: 'Flood',
          year: 1990
        },
          {
            name: 'Factory Showroom',
            year: 1996
          },
          {
            name: 'The Spine',
            year: 2004
          }]

      },
      {
        name: 'The Offspring',
        yearEstablished: 1984,
        vocalist: 'Dexter Holland',
        guitarist: 'Dexter Holland',
        rhythmGuitar: 'Dexter Holland',
        guitarist2: 'Noodles',
        rhythmGuitar2: 'Noodles',
        vocalist2: 'Noodles',
        drums: 'Pete Parada',
        vocalist3: 'Todd Morse',
        bass: 'Todd Morse',
        albums: [{
          name: 'The Offspring',
          year: 1989
        },
          {
            name: 'Americana',
            year: 1998
          },
          {
            name: 'Rise and Fall, Rage and Grace',
            year: 2008
          }]
      }
      ]
    //also old
    // this.addBand()
    this.newBand = {
      name:'',
      yearEstablished: null,
      vocalist: '',
      vocalist2: '',
      guitarist: '',
      bass: '',
      drums: '',
      albums: [],
    }
    this.newAlbum1 = {
      name: '',
      year: null
    }
    this.newAlbum2 = {
      name: '',
      year: null
    }
    this.newAlbum3 = {
      name: '',
      year: null
    }
  }

  showForm(){
    this.toggler = !this.toggler
  }

  addNewBand(that){

    this.newBand.albums[0] = this.newAlbum1
    this.newBand.albums[1] = this.newAlbum2
    this.newBand.albums[2] = this.newAlbum3

    this.bands.push(this.newBand)

    this.newBand = {
      name:'',
      yearEstablished: null,
      vocalist: '',
      vocalist2: '',
      vocalist3:'',
      guitarist: '',
      guitarist2:'',
      guitarist3:'',
      bass: '',
      bass2: '',
      bass3:'',
      drums: '',
      drums2:'',
      drums3:'',
      albums: [],
      rhythmGuitar:'',
      rhythmGuitar2:'',
      rhythmGuitar3:'',
      keyboard:'',
      keyboard2:'',
      keyboard3:'',
      saxophone:'',
      synth:'',
      saxophone2:'',
      saxophone3:'',
      synth2:'',
      synth3:'',
    }
    this.newAlbum1 = {
      name: '',
      year: null
    }
    this.newAlbum2 = {
      name: '',
      year: null
    }
    this.newAlbum3 = {
      name: '',
      year: null
    }

  }


/* ================================================================ old version
  addBand() {
    var name = prompt('new band name');
    var yearEstablished = prompt('new year established');
    var vocalist = prompt('new vocalist');
    var vocalist2 = prompt('new vocalist(2) (leave blank for none)');
    var vocalist3 = prompt('new vocalist(3) (leave blank for none)');
    var guitarist = prompt('new guitarist');
    var guitarist2 = prompt('new guitarist(2) (leave blank for none)');
    var guitarist3 = prompt('new guitarist(3) (leave blank for none)');
    var bass = prompt('new bassist');
    var drums = prompt('new drummer');
    var keyboard = prompt('new keyboardist (leave blank for none)');
    var keyboard2 = prompt('new keyboardist(2) (leave blank for none)');
    var keyboard3 = prompt('new keyboardist(3) (leave blank for none)');
    var synth = prompt('new synthesizer (leave blank for none)');
    var synth2 = prompt('new synthesizer(2) (leave blank for none)');
    var synth3 = prompt('new synthesizer(3) (leave blank for none)');
    var saxophone = prompt('new saxophone (leave blank for none)');
    var rhythmGuitar = prompt('new rhythm guitar (leave blank for none)');
    var rhythmGuitar2 = prompt('new rhythm guitar(2) (leave blank for none)');
    var albumName1 = prompt('new album 1 of 3 name');
    var albumName2 = prompt('new album 2 of 3 name');
    var albumName3 = prompt('new album 3 of 3 name');
    var albumYear1 = prompt('new album 1 of 3 release year');
    var albumYear2 = prompt('new album 2 of 3 release year');
    var albumYear3 = prompt('new album 3 of 3 release year');
    var newalbums = [
      {
        name: albumName1,
        year: albumYear1
      },
      {
        name: albumName2,
        year: albumYear2
      },
      {
        name: albumName3,
        year: albumYear3
      }
    ]

    this.bands.push({
      name: name,
      yearEstablished: parseInt(yearEstablished),
      vocalist: vocalist,
      vocalist2: vocalist2,
      vocalist3: vocalist3,
      guitarist: guitarist,
      guitarist2: guitarist2,
      guitarist3: guitarist3,
      bass: bass,
      drums: drums,
      keyboard: keyboard,
      keyboard2: keyboard2,
      keyboard3: keyboard3,
      synth: synth,
      synth2: synth2,
      synth3: synth3,
      saxophone: saxophone,
      rhythmGuitar: rhythmGuitar,
      rhythmGuitar2: rhythmGuitar2,
      albums: newalbums

    });

    // console.log(name)
    // console.log(newalbums)
    // console.log(this.bands[0])


    var findBlanks = this.bands[this.bands.length - 1];
    // console.log(findBlanks);
    var d = 0;

    if (findBlanks.vocalist2 === ''){
      delete findBlanks.vocalist2
    }

    if (findBlanks.vocalist3 === ''){
      delete findBlanks.vocalist3
    }

    if (findBlanks.guitarist2 === ''){
      delete findBlanks.guitarist2
    }

    if (findBlanks.guitarist3 === ''){
      delete findBlanks.guitarist3
    }

    if (findBlanks.keyboard === ''){
      delete findBlanks.keyboard
    }

    if (findBlanks.keyboard2 === ''){
      delete findBlanks.keyboard2
    }

    if (findBlanks.keyboard3 === ''){
      delete findBlanks.keyboard3
    }

    if (findBlanks.synth === ''){
      delete findBlanks.synth
    }

    if (findBlanks.synth2 === ''){
      delete findBlanks.synth2
    }

    if (findBlanks.synth3 === ''){
      delete findBlanks.synth3
    }
    if (findBlanks.saxophone === ''){
      delete findBlanks.saxophone
    }
    if (findBlanks.rhythmGuitar === ''){
      delete findBlanks.rhythmGuitar
    }
    if (findBlanks.rhythmGuitar2 === ''){
      delete findBlanks.rhythmGuitar2
    }

  }


 ==========================================================================================  */

}
