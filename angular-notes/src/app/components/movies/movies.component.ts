import { Component, OnInit } from '@angular/core';
import {Movie} from "../../models/Movie";

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  movieList: Movie[];
  // classvar: {};

  constructor() { }

  ngOnInit(): void {
    //movie array ======================================================
    this.movieList = [
      {
        title: 'Star Trek VI: The Undiscovered Country',
        yearReleased: 1992,
        actors:[' William Shatner', ' Leonard Nimoy', ' DeForest Kelley'],
        director:'Nicholas Meyer',
        genre:[" Action", " Adventure", " Sci-Fi"],
        rating: 'PG',
        awards:[
          {
          type:false,
          award:'Oscar',
          category:'Best Effects, Sound Effects Editing',
            year: 1992
        },
          {
            type: false,
            award: 'Oscar',
            category: 'Best Makup',
            year: 1992
          },
          {
            type:true,
            award: 'Saturn Award',
            category: 'Best Science Fiction Film',
            year: 1993
          },
          {
            type:false,
            award: 'Saturn Award',
            category: 'Best Supporting Actress',
            year: 1993
          },
          {
            type:false,
            award: 'Saturn Award',
            category: 'Best Writing',
            year: 1993
          },
          {
            type:false,
            award: 'Saturn Award',
            category: 'Best Writing',
            year: 1993
          },
          {
            type:false,
            award: 'Saturn Award',
            category: 'Best Costumes',
            year: 1993
          },
          {
            type:false,
            award: 'Saturn Award',
            category: 'Best Make-Up',
            year: 1993
          },
          {
            type:true,
            award: 'Saturn Award',
            category: 'Best DVD Collection',
            year: 2010
          },
          {
            type:false,
            award: 'ACCA',
            category: 'Best Visual Effects',
            year: 1991
          },
          {
            type:false,
            award: 'ACCA',
            category: 'Best Makeup & Hairstyling',
            year: 1991
          },
          {
            type:false,
            award: 'Hugo',
            category: 'Best Dramatic Presentation',
            year: 1992
          }

        ],
        img: '../../assets/img/st6.jpg'
      },

      {
        title: 'Short Circuit 2',
        yearReleased: 1988,
        actors: [" Fisher Stevens", " Michael McKean", " Tim Blaney"],
        director: 'Kenneeth Johnson',
        genre: [" Comedy", " Drama", " Family"],
        rating:'PG',
        awards:[
          {
            type: false,
            award: 'Saturn Award',
            category: 'Best Science Fiction Film',
            year: 1990,
          },
          {
            type: false,
            award: 'Saturn Award',
            category: 'Best Special Effects',
            year: 1990
          }
        ],
        img:'../../assets/img/sc2.jpg'
      },
      {
        title: 'Titan A.E.',
        yearReleased: 2000,
        actors: [' Matt Damon', ' Drew Barrymore', ' Bill Pullman'],
        director: 'Don Bluth & Gary Goldman',
        genre: [" Animation", "Action", "Adventure"],
        rating: 'PG',
        awards:[
          {
            type: false,
            award: 'Saturn Award',
            category: 'Best Science Fiction Film',
            year: 2001
          },
          {
            type: false,
            award: 'Annie',
            category: 'Outstanding Individual Achievement for Effects Animation',
            year: 2000
          },
          {
            type: false,
            award: 'Annie',
            category: 'Outstanding Individual Achievement for Production Design in an Animated Feature Production',
            year: 2000
          },
          {
            type: false,
            award: 'Annie',
            category: 'Outstanding Achievement in an Animated Theatrical Feature',
            year: 2000
          },
          {
            type: true,
            award: 'Golden Reel Award',
            category: 'Best Sound Editing - Animated Feature',
            year: 2001
          },
          {
            type: false,
            award: 'Golden Reel Award',
            category: 'Best Sound Editing - Music - Animation',
            year: 2001
          },
          {
            type: false,
            award: 'Golden Satellite Award',
            category: 'Best Motion Picture, Animated or Mixed Media',
            year: 2001
          }
        ],
        img:'../../assets/img/tae.jpg'
      }
    ] //end of array =======================================

    this.addmovieOld()

    }







    // add movie function ------------------------------------------------
    addmovieOld(){
    var title = 'Galaxy Quest';
    var yearReleased = 1999;
    var actors = [' Tim Allen', ' Sigourney Weaver', ' Alan Rickman'];
    var director = 'Dean Parisot';
    var genre = [' Action', 'Comedy', 'Sci-Fi'];
    var rating = 'PG';
    var awards = [
      {
        type: true,
        award: 'Saturn Award',
        category:'Best Actor',
        year:2000
      },
      {
        type: false,
        award: 'Saturn Award',
        category:'Best Science Fiction Film',
        year:2000
      },
      {
        type: false,
        award: 'Saturn Award',
        category:'Best Actress',
        year:2000
      },
      {
        type: false,
        award: 'Saturn Award',
        category:'Best Supporting Actor',
        year:2000
      },
      {
        type: false,
        award: 'Saturn Award',
        category:'Best Performance by a Younger Actor/Actress',
        year:2000
      },
      {
        type: false,
        award: 'Saturn Award',
        category:'Best Director',
        year:2000
      },
      {
        type: false,
        award: 'Saturn Award',
        category:'Best Music',
        year:2000
      },
      {
        type: false,
        award: 'Saturn Award',
        category:'Best Costumes',
        year:2000
      },
      {
        type: false,
        award: 'Saturn Award',
        category:'Best Make-Up',
        year:2000
      },
      {
        type: false,
        award: 'Saturn Award',
        category:'Best Special Effects',
        year:2000
      },
      {
        type: true,
        award: 'Silver Scream Award',
        category:'The Amsterdam Fantastic Film Festival',
        year:2000
      },
      {
        type: false,
        award: 'Blockbuster Entertainment Award',
        category:'Favorite Actress - Comedy',
        year:2000
      },
      {
        type: false,
        award: 'Blockbuster Entertainment Award',
        category:'Favorite Actor - Comedy',
        year:2000
      },
      {
        type: true,
        award: 'Pegasus Audience Award',
        category:'Brussels International Festival of Fantasy Film (BIFFF)',
        year:2000
      },
      {
        type: true,
        award: 'Silver Raven',
        category:'Best Screenplay',
        year:2000
      },
      {
        type: false,
        award: 'Artios',
        category:'Best Casting for Feature Film, Comedy',
        year:2000
      },
      {
        type: true,
        award: 'Hochi',
        category:'Best Foreign Language Film',
        year:2001
      },
      {
        type: true,
        award: 'Hugo',
        category:'Best Dramatic Presentation',
        year:2000
      },
      {
        type: false,
        award: 'Sierra Award',
        category:'Best Visual Effects',
        year:2000
      },
      {
        type: true,
        award: 'Nebula Award',
        category:'Best Script',
        year:2001
      },
      {
        type: false,
        award: 'Teen Choice Award',
        category:'Film - Choice Comedy',
        year:2000
      },
    ];
    var img = '../../assets/img/gq.jpg';

    this.movieList.push({
      title: title,
      yearReleased: yearReleased,
      actors:actors,
      director:director,
      genre:genre,
      rating:rating,
      awards:awards,
      img:img
    });
  }
  //------------------------------------------------

  // setClassVar(){
  //
  // }

}
