import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { UserComponent} from "./components/user.component";
import { helloComponent} from "./components/hello.component";
import { blog } from "./components/travel/your";
import { UsersComponent } from './components/users/users.component';
import { MathComponent } from './components/math/math.component';
import { VipComponent } from './components/vip/vip.component';
import { BandsComponent } from './components/bands/bands.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialComponent } from './components/material/material.component';

//angular material modules
import {MatSliderModule} from "@angular/material/slider";

import {MatFormFieldModule} from "@angular/material/form-field";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";

import {MatNativeDateModule} from "@angular/material/core";

import {MatRadioButton} from "@angular/material/radio";
import {MatRadioButtonHarness} from "@angular/material/radio/testing";
import {MatRadioModule} from "@angular/material/radio";
import {MatGridListModule} from "@angular/material/grid-list";

// ------------
import { MoviesComponent } from './components/movies/movies.component';
import { BindingComponent } from './components/binding/binding.component';
import { EventsComponent } from './components/events/events.component';
import { NgmodelComponent } from './components/ngmodel/ngmodel.component';


import {FormsModule} from "@angular/forms";
import { PostHttpClientComponent } from './components/post-http-client/post-http-client.component';
import {HttpClientModule} from "@angular/common/http";


import {MatCardModule} from "@angular/material/card";
import { PostFormComponent } from './components/post-form/post-form.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';

@NgModule({
  declarations: [
    // components goes here
    AppComponent, UserComponent,
    helloComponent,
    blog,
    UsersComponent,
    MathComponent,
    VipComponent,
    BandsComponent,
    MaterialComponent,
    MoviesComponent,
    BindingComponent,
    EventsComponent,
    NgmodelComponent,
    PostHttpClientComponent,
    PostFormComponent,
    HomeComponent,
    NavbarComponent,

  ],
  imports: [
    // modules goes here
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule, MatFormFieldModule, MatDatepickerModule, MatInputModule, MatSelectModule, MatNativeDateModule,
    // MatRadioButton, MatRadioButtonHarness
    MatRadioModule,
    MatGridListModule,
    FormsModule,
    HttpClientModule,
    MatCardModule,
  ],
  providers: [
    //services goes here
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
