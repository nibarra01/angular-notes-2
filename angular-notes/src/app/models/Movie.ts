export interface Movie{
  title:string,
  yearReleased:number,
  actors:any,
  director:string,
  genre:any,
  rating:string,
  awards:any,
  img:string
}
